const Course = require('../models/Course');
const User = require('../models/User');

// Create a new course
module.exports.addCourse = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin == false) {
            return "Sorry, you are not an Admin"
        } else {

            // create a variable "newCourse" and instantiate a new "Course" object using the mongoose model.
            // uses the information from the request body to provide all the necessary information.
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            });

            // saves the created object to our DB using .save()
            return newCourse.save().then((course, error) => {
                // Course creation failed
                if (error) {
                    return false
                    // Course creation successful
                } else {
                    return "Course successfully created"
                }
            })
        }
    })
};

// controller function for restrieving all courses
module.exports.getAllCourses = (data) => {
    if(data.isAdmin) {
        return Course.find({}).then(result => {
            return result
        })
    } else {
        return false
    }
}; 

// retrieve all active courses
module.exports.getAllActive = () => {
    return Course.find({isActive: true}).then(result => {
        return result
    })
};


// retrieve a specific course

module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result
    })
};


// 

module.exports.updateCourse = (reqParams, reqBody, data) =>{

    if(data) {

        let updatedCourse = {
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        }
            // Syntax: findByIdAndUpdate(document ID, updatesToBeApplied)
                //
        return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {
            if(error) {
                return false
            } else {
                return true
            }
        })

    } else {
        return "You are not an Admin!"
    }
};


// archive courses

module.exports.archiveCourse = (reqParams, reqBody, data) => {

    if(data) {
        let archivedCourse = {
            isActive: reqBody.isActive
        }

        return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((archivedCourse,error) => {
            if(error) {
                return false
            } else {
                return true
            }
        })

    } else {
        return "You are not an Admin!"
    }
};


// instructor solution
/*module.exports.archiveCourse = (data, reqBody) => {
    if (data.isAdmin === true) {
        let updateActiveField = {
            isActive: reqBody.isActive
        }

        return Course.findByIdAndUpdate(data.courseId, updateActiveField).then((archivedCourse,error) => {
            if(error) {
                return false
            } else {
                return true
            }
        })
    } 
}*/


