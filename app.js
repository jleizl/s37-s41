// Express Setup
const express = require('express');
const mongoose = require('mongoose');

// allows our backend application to be available for use in our frontend application
// allows us to control the app's Cross-Origin Resources Sharing Settings
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');

const app = express();

// process.env.PORT || 4000 means that whatever is in the enviroment(Heroku) variable PORT, or 4000  ther server will connect or listen to. 
const port = process.env.PORT || 4000;

// Middlewares:
// Allows all resources to access our backen application
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Main URI:
app.use("/users",userRoutes)
app.use("/courses",courseRoutes)

// Mongoose Connection

mongoose.connect(`mongodb://jleizl93:admin123@ac-floig6u-shard-00-00.2zb8dt2.mongodb.net:27017,ac-floig6u-shard-00-01.2zb8dt2.mongodb.net:27017,ac-floig6u-shard-00-02.2zb8dt2.mongodb.net:27017/s37-s41?ssl=true&replicaSet=atlas-12lysg-shard-0&authSource=admin&retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

const db = mongoose.connection

db.on('error', () => console.error('Connection Error'))
db.once('open', () => console.log('Connected to MongoDB!') )
app.listen(port, () => console.log(`API is now online at port: ${port}`));