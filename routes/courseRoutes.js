const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require('../auth');



// Route for creating a course
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	
	courseController.addCourse(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => res.send(resultFromController))
});


// Route for getting all the courses

router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
});


// route for retrieving all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => 
		res.send(resultFromController))
	});


// route for retrieving specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId)
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
});

// route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {

	const isAdminData = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdminData)
	console.log(req.params.courseId)

	courseController.updateCourse(req.params, req.body, isAdminData).then(resultFromController => res.send(resultFromController))
});

// activity
// route for archiving a course
// my solution

router.put("/:courseId/archive", auth.verify, (req, res) => {

	const isAdminData = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdminData)
	courseController.archiveCourse(req.params, req.body, isAdminData).then(resultFromController => res.send(resultFromController))

});

/*// instructor solution
router.put("/:courseId/archive", auth.verify, (req, res) => {

	const data = {
		courseId : req.params.courseId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	
	courseController.archiveCourse(data, req.body).then(resultFromController => res.send(resultFromController))

});*/



module.exports = router; 


