const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

// Routes:

// checkEmail route -checks if email is existing from our DB.
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// register route - create user in our DB collection.
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then((resultFromController) => res.send(resultFromController))
});

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	console.log(userData)

	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController));
});

// route for enrolling a user
router.post("/enroll", (req, res) => {
	let data = {
		userId: req.body.userId, 
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
});


// activity s41

router.post("/enrollAct", auth.verify, (req, res) => {

	const isAdminData = auth.decode(req.headers.authorization).isAdmin

	console.log(isAdminData)

	let data = {
		userId: req.body.userId, 
		courseId: req.body.courseId
	}

	userController.enrollAct(data).then(resultFromController => res.send(resultFromController))
});


// S41 activity instructor's solution

router.post("/enroll2", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);



	let data = {
		userId: userData.id, 
		isAdmin: userData.isAdmin,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
});


module.exports = router;